package com.luncan.razvan.assignment1.luncanlazvanassignment1.entities;

public class CareGiverPatient {

    private Integer careGiverID;
    private Integer patientID;

    public CareGiverPatient(Integer careGiverID, Integer patientID){
        this.careGiverID = careGiverID;
        this.patientID = patientID;
    }

    public Integer getCareGiverID() {
        return careGiverID;
    }

    public void setCareGiverID(Integer careGiverID) {
        this.careGiverID = careGiverID;
    }

    public Integer getPatientID() {
        return patientID;
    }

    public void setPatientID(Integer patientID) {
        this.patientID = patientID;
    }
}
