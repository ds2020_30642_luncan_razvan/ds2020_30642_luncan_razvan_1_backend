package com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {
}
