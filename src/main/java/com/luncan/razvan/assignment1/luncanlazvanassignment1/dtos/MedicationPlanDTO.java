package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

import java.sql.Date;

public class MedicationPlanDTO {

    private String intakeIntervals;
    private Date beginningDate;
    private Date endingDate;
    private StringBuilder medicationSet;

    public MedicationPlanDTO(String intakeIntervals,
                             Date beginningDate,
                             Date endingDate,
                             StringBuilder medicationSet){
        this.intakeIntervals = intakeIntervals;
        this.beginningDate = beginningDate;
        this.endingDate = endingDate;
        this.medicationSet = medicationSet;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public Date getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(Date beginningDate) {
        this.beginningDate = beginningDate;
    }

    public Date getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(Date endingDate) {
        this.endingDate = endingDate;
    }

    public StringBuilder getMedicationSet() {
        return medicationSet;
    }

    public void setMedicationSet(StringBuilder medicationSet) {
        this.medicationSet = medicationSet;
    }
}
