package com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicalRepository extends JpaRepository<Medication, Integer> {
}
