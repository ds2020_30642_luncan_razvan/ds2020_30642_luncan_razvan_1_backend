package com.luncan.razvan.assignment1.luncanlazvanassignment1.controllers;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.CareGiverDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService){this.medicationService = medicationService;}

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedications(){
        List<MedicationDTO> dtos = medicationService.findMedication();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Integer> insertMedication(@RequestBody MedicationDetailsDTO medicationDetailsDTO){
        Integer medicationID = medicationService.insert(medicationDetailsDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @RequestMapping(method = PUT)
    public ResponseEntity<Integer> updatePatient(@Valid @RequestBody MedicationDetailsDTO medicationDetailsDTO){
        Integer medicationID = medicationService.update(medicationDetailsDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity<Integer> deleteMedication(@PathVariable("id") Integer medicationID){
        medicationService.deleteMedicationByID(medicationID);
        return new ResponseEntity<>(medicationID, HttpStatus.OK);
    }

}
