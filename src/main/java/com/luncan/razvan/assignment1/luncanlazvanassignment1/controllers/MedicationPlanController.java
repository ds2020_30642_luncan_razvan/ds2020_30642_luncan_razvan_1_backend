package com.luncan.razvan.assignment1.luncanlazvanassignment1.controllers;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationPlanDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.services.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService){
        this.medicationPlanService = medicationPlanService;
    }

    @PostMapping()
    @Transactional
    public ResponseEntity<Integer> insertMedicationPlan(@RequestBody MedicationPlanDetailsDTO medicationPlanDetailsDTO){
        Integer medicationPlanID = medicationPlanService.insert(medicationPlanDetailsDTO);
        return new ResponseEntity<>(medicationPlanID, HttpStatus.CREATED);
    }

}
