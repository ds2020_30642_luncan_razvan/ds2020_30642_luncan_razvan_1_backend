package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.PatientDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.PatientDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.UserDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.UserDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Patient;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.User;

public class PatientBuilder {

    private PatientBuilder(){

    }

    public static PatientDTO toPatientDTO(Patient patient){
        return new PatientDTO(patient.getId(),
                patient.getName(),
                patient.getBirthDate(),
                patient.getGender(),
                patient.getAddress(),
                patient.getMedicalRecord(),
                patient.getRole());
    }

    public static Patient toEntity(PatientDetailsDTO patientDetailsDTO){
        return new Patient(patientDetailsDTO.getName(),
                patientDetailsDTO.getBirthDate(),
                patientDetailsDTO.getGender(),
                patientDetailsDTO.getAddress(),
                patientDetailsDTO.getMedicalRecord(),
                "patient",
                patientDetailsDTO.getUserName(),
                patientDetailsDTO.getPassword());
    }

    public static Patient toEntityUpdate(PatientDetailsDTO patientDetailsDTO){
       Patient patient = new Patient();
       patient.setId(patientDetailsDTO.getId());
       patient.setName(patientDetailsDTO.getName());
       patient.setBirthDate(patientDetailsDTO.getBirthDate());
       patient.setGender(patientDetailsDTO.getGender());
       patient.setAddress(patientDetailsDTO.getAddress());
       patient.setMedicalRecord(patientDetailsDTO.getMedicalRecord());
       return patient;
    }
}
