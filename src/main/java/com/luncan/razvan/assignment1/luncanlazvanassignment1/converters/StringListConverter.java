package com.luncan.razvan.assignment1.luncanlazvanassignment1.converters;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Patient;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.List;

@Converter
public class StringListConverter implements AttributeConverter<List<Patient>, String> {

    private static final String SPLIT_CHAR = ";";

    @Override
    public String convertToDatabaseColumn(List<Patient> patients) {

        StringBuilder s = new StringBuilder();

        for(Patient patient: patients){
            s.append(patient.getName()).append(SPLIT_CHAR);
        }

        return s.toString();
    }

    @Override
    public List<Patient> convertToEntityAttribute(String s) {
        return null;
    }
}
