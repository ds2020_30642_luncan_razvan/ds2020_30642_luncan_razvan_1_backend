package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class MedicationDetailsDTO {

    private int id;
    @NotNull
    private String name;
    @NotNull
    private String sideEffects;
    @NotNull
    private int dosage;

    public MedicationDetailsDTO(){

    }

    public MedicationDetailsDTO(int id, String name, String sideEffects, int dosage){
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public MedicationDetailsDTO(String name, String sideEffects, int dosage){
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    @Override
    public String toString() {
        return "MedicationDetailsDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sideEffects='" + sideEffects + '\'' +
                ", dosage=" + dosage +
                '}';
    }
}
