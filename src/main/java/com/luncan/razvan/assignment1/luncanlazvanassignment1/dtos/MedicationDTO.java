package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

import java.util.Objects;

public class MedicationDTO {

    private int id;
    private String name;
    private String sideEffects;
    private int dosage;

    public MedicationDTO(int id, String name, String sideEffects, int dosage){
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO that = (MedicationDTO) o;
        return dosage == that.dosage &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(sideEffects, that.sideEffects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, sideEffects, dosage);
    }
}
