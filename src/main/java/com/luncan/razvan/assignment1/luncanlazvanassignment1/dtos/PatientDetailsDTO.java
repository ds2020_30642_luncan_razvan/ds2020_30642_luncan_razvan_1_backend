package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.UUID;

public class PatientDetailsDTO extends UserDetailsDTO {

    @NotNull
    private String medicalRecord;

    public PatientDetailsDTO(){

    }

    public PatientDetailsDTO(int id, String name, Date birthDate, String gender, String address, String medicalRecord, String role){
        super(id, name, birthDate, gender, address, role);
        this.medicalRecord = medicalRecord;
    }

    public PatientDetailsDTO(String name, Date birthDate, String gender, String address, String medicalRecord, String role, String userName, String password){
        super(name, birthDate, gender, address, role, userName, password);
        this.medicalRecord = medicalRecord;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    @Override
    public String toString() {

        return  super.toString() + "PatientDetailsDTO{" +
                "medicalRecord='" + medicalRecord + '\'' +
                '}';
    }
}
