package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.UUID;

public class UserDetailsDTO {

    private int id;
    @NotNull
    private String name;
    @NotNull
    private Date birthDate;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    private String role;
    private String userName;
    private String password;

    public UserDetailsDTO(){

    }

    public UserDetailsDTO(String name, Date birthDate, String gender, String address, String role, String userName, String password){
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.role = role;
        this.userName = userName;
        this.password = password;
    }

    public UserDetailsDTO(int id, String name, Date birthDate, String gender, String address, String role){
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.role = role;
    }

    public UserDetailsDTO(int id, String name, Date birthDate, String gender, String address, String role, String userName, String password){
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.role = role;
        this.userName = userName;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserDetailsDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthDate=" + birthDate +
                ", gender='" + gender + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
