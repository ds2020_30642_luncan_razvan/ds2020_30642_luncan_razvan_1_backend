package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CareGiverDetailsDTO extends UserDetailsDTO {

    private List<String> patients;

    public CareGiverDetailsDTO(){

    }

    public CareGiverDetailsDTO(String name, Date birthDate, String gender, String address, String role, String userName, String password){
        super(name, birthDate, gender, address, role, userName, password);
        this.patients = new ArrayList<>();
    }

    public CareGiverDetailsDTO(int id, String name, Date birthDate, String gender, String address, String role){
        super(id, name, birthDate, gender, address, role);
        this.patients = new ArrayList<>();
    }

    public List<String> getPatients() {
        return patients;
    }

    public void setPatients(List<String> patients) {
        this.patients = patients;
    }

}
