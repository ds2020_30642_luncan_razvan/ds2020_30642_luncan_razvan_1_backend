package com.luncan.razvan.assignment1.luncanlazvanassignment1.services;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.CareGiverDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.CareGiverDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.PatientDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.PatientDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders.CareGiverBuilder;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders.PatientBuilder;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.CareGiver;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Patient;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.CareGiverRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CareGiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CareGiverService.class);
    private final CareGiverRepository careGiverRepository;

    @Autowired
    public CareGiverService(CareGiverRepository careGiverRepository){this.careGiverRepository = careGiverRepository;}

    public List<CareGiverDTO> findCareGivers(){
        List<CareGiver> careGiverList = careGiverRepository.findAll();
        return careGiverList.stream()
                .map(CareGiverBuilder::toCareGiverDTO)
                .collect(Collectors.toList());
    }

    public Integer insert(CareGiverDetailsDTO careGiverDetailsDTO){
        CareGiver careGiver = CareGiverBuilder.toEntity(careGiverDetailsDTO);
        careGiver = careGiverRepository.save(careGiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", careGiver.getId());
        return careGiver.getId();
    }

    public Integer update(CareGiverDetailsDTO careGiverDetailsDTO){
        CareGiver careGiver = CareGiverBuilder.toEntityUpdate(careGiverDetailsDTO);
        CareGiver careGiver1 = careGiverRepository.findById(careGiver.getId()).get();
        careGiver.setRole(careGiver1.getRole());
        careGiver.setUserName(careGiver1.getUserName());
        careGiver.setPassword(careGiver1.getPassword());
        careGiver.setPatients(careGiver1.getPatients());
        careGiver = careGiverRepository.save(careGiver);
        LOGGER.debug("Patient with id {} was updates in db", careGiver.getId());
        return careGiver.getId();
    }

    public void deleteCareGiverByID(Integer id){
        careGiverRepository.deleteById(id);
    }

    public List<PatientDTO> getCareGiversPatients(Integer id){
        List<Patient> patientList = careGiverRepository.findById(id).get().getPatients();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }
}
