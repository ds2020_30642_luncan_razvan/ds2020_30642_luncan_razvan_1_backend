package com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.CareGiver;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CareGiverRepository extends JpaRepository<CareGiver, Integer> {

    CareGiver findCareGiverByUserNameAndPassword(String userName, String password);

}
