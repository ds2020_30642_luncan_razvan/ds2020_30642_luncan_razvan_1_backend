package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.CareGiverPatientDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.CareGiverPatient;

public class CareGiverPatientBuilder {

    public static CareGiverPatient toEntity(CareGiverPatientDetailsDTO careGiverPatientDetailsDTO){
        return new CareGiverPatient(careGiverPatientDetailsDTO.getCareGiverID(),
                careGiverPatientDetailsDTO.getPatientID());
    }

}
