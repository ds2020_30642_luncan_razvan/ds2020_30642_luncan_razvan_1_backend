package com.luncan.razvan.assignment1.luncanlazvanassignment1.controllers;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.*;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.CareGiver;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.services.CareGiverService;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService, CareGiverService careGiverService){
        this.patientService = patientService;
    }

    @GetMapping()
    public ResponseEntity<List<PatientDTO>> getPatients(){
        List<PatientDTO> dtos = patientService.findPatients();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/medicationPlanPatients/{id}")
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlanForPatient(@PathVariable("id") Integer patientID){
        List<MedicationPlanDTO> medicationPlanDTOList = patientService.getMedicationPlanFromPatientByID(patientID);
        return new ResponseEntity<>(medicationPlanDTOList, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Integer> insertPatient(@Valid @RequestBody PatientDetailsDTO patientDetailsDTO){

        Integer patientID = patientService.insert(patientDetailsDTO);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }

    @PostMapping(value = "/assignCareGiver")
    public ResponseEntity<Integer> assignCareGiver(@Valid @RequestBody CareGiverPatientDetailsDTO careGiverPatientDetailsDTO){
        Integer status = patientService.assignCareGiver(careGiverPatientDetailsDTO);
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    @RequestMapping(method = PUT)
    public ResponseEntity<Integer> updatePatient(@Valid @RequestBody PatientDetailsDTO patientDetailsDTO){
        Integer patientID = patientService.update(patientDetailsDTO);
        return new ResponseEntity<>(patientID, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity<Integer> deletePatient(@PathVariable("id") Integer patientID){
        patientService.deletePatientByID(patientID);
        return new ResponseEntity<>(patientID, HttpStatus.OK);
    }

}
