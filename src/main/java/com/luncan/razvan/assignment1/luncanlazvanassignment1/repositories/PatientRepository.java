package com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.CareGiver;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

    Patient findPatientByUserNameAndPassword(String userName, String password);

}
