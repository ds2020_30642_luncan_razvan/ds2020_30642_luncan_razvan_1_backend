package com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
