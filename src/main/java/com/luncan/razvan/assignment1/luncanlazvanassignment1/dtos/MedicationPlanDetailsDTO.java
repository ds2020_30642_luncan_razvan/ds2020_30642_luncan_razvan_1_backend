package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Medication;

import java.sql.Date;
import java.util.List;

public class MedicationPlanDetailsDTO {

    private Integer patientID;
    private String intakeIntervals;
    private Date beginningDate;
    private Date endingDate;
    private List<Integer> medicationIDsList;
    private List<Medication> medications;

    public MedicationPlanDetailsDTO(Integer patientID,
                                    String intakeIntervals,
                                    Date beginningDate, Date endingDate,
                                    List<Integer> medicationIDsList) {
        this.patientID = patientID;
        this.intakeIntervals = intakeIntervals;
        this.beginningDate = beginningDate;
        this.endingDate = endingDate;
        this.medicationIDsList = medicationIDsList;

    }

    public Integer getPatientID() {
        return patientID;
    }

    public void setPatientID(Integer patientID) {
        this.patientID = patientID;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public Date getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(Date beginningDate) {
        this.beginningDate = beginningDate;
    }

    public Date getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(Date endingDate) {
        this.endingDate = endingDate;
    }

    public List<Integer> getMedicationIDsList() {
        return medicationIDsList;
    }

    public void setMedicationIDsList(List<Integer> medicationIDsList) {
        this.medicationIDsList = medicationIDsList;
    }

    @Override
    public String toString() {
        return "MedicationPlanDetailsDTO{" +
                "patientID=" + patientID +
                ", intakeIntervals='" + intakeIntervals + '\'' +
                ", beginningDate=" + beginningDate +
                ", endingDate=" + endingDate +
                ", medicationIDsList=" + medicationIDsList +
                '}';
    }
}
