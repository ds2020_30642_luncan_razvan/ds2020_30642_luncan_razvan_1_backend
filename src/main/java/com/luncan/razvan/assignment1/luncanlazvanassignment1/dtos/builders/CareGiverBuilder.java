package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.CareGiverDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.CareGiverDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.CareGiver;


public class CareGiverBuilder {

    private CareGiverBuilder(){

    }

    public static CareGiverDTO toCareGiverDTO(CareGiver careGiver){
        return new CareGiverDTO(careGiver.getId(),
                careGiver.getName(),
                careGiver.getBirthDate(),
                careGiver.getGender(),
                careGiver.getAddress(),
                careGiver.getRole());
    }

    public static CareGiver toEntity(CareGiverDetailsDTO careGiverDetailsDTO){
        return new CareGiver(careGiverDetailsDTO.getName(),
                careGiverDetailsDTO.getBirthDate(),
                careGiverDetailsDTO.getGender(),
                careGiverDetailsDTO.getAddress(),
                "careGiver",
                careGiverDetailsDTO.getUserName(),
                careGiverDetailsDTO.getPassword());
    }

    public static CareGiver toEntityUpdate(CareGiverDetailsDTO careGiverDetailsDTO){
        CareGiver careGiver = new CareGiver();
        careGiver.setId(careGiverDetailsDTO.getId());
        careGiver.setName(careGiverDetailsDTO.getName());
        careGiver.setBirthDate(careGiverDetailsDTO.getBirthDate());
        careGiver.setGender(careGiverDetailsDTO.getGender());
        careGiver.setAddress(careGiverDetailsDTO.getAddress());
        return careGiver;
    }

}
