package com.luncan.razvan.assignment1.luncanlazvanassignment1.entities;

public class LoginDetails {

    private String userNameLogin;
    private String passwordLogin;

    public LoginDetails(String userNameLogin, String passwordLogin){
        this.userNameLogin = userNameLogin;
        this.passwordLogin = passwordLogin;
    }

    public String getUserNameLogin() {
        return userNameLogin;
    }

    public void setUserNameLogin(String userNameLogin) {
        this.userNameLogin = userNameLogin;
    }

    public String getPasswordLogin() {
        return passwordLogin;
    }

    public void setPasswordLogin(String passwordLogin) {
        this.passwordLogin = passwordLogin;
    }
}
