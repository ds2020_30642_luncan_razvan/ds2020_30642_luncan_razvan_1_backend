package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Medication;

public class MedicationBuilder {

    private MedicationBuilder(){

    }

    public static MedicationDTO toMedicationDTO(Medication medication){
        return new MedicationDTO(medication.getId(), medication.getName(), medication.getSideEffects(), medication.getDosage());
    }

    public static Medication toEntity(MedicationDetailsDTO medicationDetailsDTO){
        return new Medication(medicationDetailsDTO.getName(),
                medicationDetailsDTO.getSideEffects(),
                medicationDetailsDTO.getDosage());
    }

    public static Medication toEntityUpdate(MedicationDetailsDTO medicationDetailsDTO){
        Medication medication = new Medication();
        medication.setId(medicationDetailsDTO.getId());
        medication.setName(medicationDetailsDTO.getName());
        medication.setSideEffects(medicationDetailsDTO.getSideEffects());
        medication.setDosage(medicationDetailsDTO.getDosage());
        return medication;
    }
}
