package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

public class CareGiverPatientDetailsDTO {

    private Integer patientID;
    private Integer careGiverID;

    public CareGiverPatientDetailsDTO(Integer patientID, Integer careGiverID){
        this.patientID = patientID;
        this.careGiverID = careGiverID;
    }

    public Integer getPatientID() {
        return patientID;
    }

    public void setPatientID(Integer patientID) {
        this.patientID = patientID;
    }

    public Integer getCareGiverID() {
        return careGiverID;
    }

    public void setCareGiverID(Integer careGiverID) {
        this.careGiverID = careGiverID;
    }
}
