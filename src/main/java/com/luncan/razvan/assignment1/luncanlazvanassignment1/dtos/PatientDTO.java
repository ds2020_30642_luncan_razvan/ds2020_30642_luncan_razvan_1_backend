package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

import java.sql.Date;
import java.util.Objects;
import java.util.UUID;

public class PatientDTO extends UserDTO {

    private String medicalRecord;

    public PatientDTO(int id, String name, Date birthDate, String gender, String address, String medicalRecord, String role){
        super(id, name, birthDate, gender, address, role);
        this.medicalRecord = medicalRecord;
    }

    public PatientDTO(String name, Date birthDate, String gender, String address, String medicalRecord, String role, String userName, String password){
        super(name, birthDate, gender, address, role, userName, password);
        this.medicalRecord = medicalRecord;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PatientDTO that = (PatientDTO) o;
        return Objects.equals(medicalRecord, that.medicalRecord) && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), medicalRecord);
    }
}
