package com.luncan.razvan.assignment1.luncanlazvanassignment1.entities;;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

@Entity
public class Patient extends User implements Serializable {

    private static final long SERIAL_VERSION_UID = 1L;

    @Column(name = "medial_record", nullable = false)
    private String medicalRecord;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "caregiver_id")
    private CareGiver careGiver;

    @OneToMany(mappedBy = "medicationPlanPatient", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private List<MedicationPlan> medicationPlans;



    public Patient(){

    }

    public Patient(String name, Date birthDate, String gender, String address, String medicalRecord, String role, String userName, String password){
        super(name, birthDate, gender, address, role, userName, password);
        this.medicalRecord = medicalRecord;
    }

    public List<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(List<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public CareGiver getCareGiver() {
        return careGiver;
    }

    public void setCareGiver(CareGiver careGiver) {
        this.careGiver = careGiver;
    }
}
