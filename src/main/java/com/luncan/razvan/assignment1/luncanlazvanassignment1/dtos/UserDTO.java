package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

import org.springframework.beans.factory.config.CustomEditorConfigurer;

import javax.persistence.Column;
import java.sql.Date;
import java.util.Objects;
import java.util.UUID;

public class UserDTO {

    private int id;
    private String name;
    private Date birthDate;
    private String gender;
    private String address;
    private String role;
    private String userName;
    private String password;

    public UserDTO(){

    }

    public UserDTO(int id, String name, Date birthDate, String gender, String address, String role){
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.role = role;
    }

    public UserDTO(String name, Date birthDate, String gender, String address, String role, String userName, String password){
        //this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.role = role;
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(id, userDTO.id) &&
                Objects.equals(name, userDTO.name) &&
                Objects.equals(birthDate, userDTO.birthDate) &&
                Objects.equals(gender, userDTO.gender) &&
                Objects.equals(address, userDTO.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthDate, gender, address);
    }
}
