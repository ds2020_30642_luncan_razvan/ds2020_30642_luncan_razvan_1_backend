package com.luncan.razvan.assignment1.luncanlazvanassignment1.entities;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.converters.StringListConverter;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
public class CareGiver extends User implements Serializable {

    private static final long SERIAL_VERSION_UID = 1L;

    @OneToMany(mappedBy = "careGiver", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @Convert(converter = StringListConverter.class)
    private List<Patient> patients;

    public CareGiver(){

    }

    public CareGiver(String name, Date birthDate, String gender, String address, String role, String userName, String password){
        super(name, birthDate, gender, address, role, userName, password);
        this.patients = new ArrayList<>();
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
