package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationPlanDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationPlanDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Medication;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.MedicationPlan;

public class MedicationPlanBuilder {

    public static MedicationPlan toEntity (MedicationPlanDetailsDTO medicationPlanDetailsDTO){
        return new MedicationPlan(medicationPlanDetailsDTO.getIntakeIntervals(),
                medicationPlanDetailsDTO.getBeginningDate(),
                medicationPlanDetailsDTO.getEndingDate());
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan){
        StringBuilder medications = new StringBuilder();
        for(Medication medication: medicationPlan.getMedicationList()){
            medications.append(medication.getName()+ " ");
        }
        return new MedicationPlanDTO(medicationPlan.getIntakeIntervals(),
                medicationPlan.getBeginningOfTreatment(),
                medicationPlan.getEndingOfTreatment(),
                medications);
    }

}

