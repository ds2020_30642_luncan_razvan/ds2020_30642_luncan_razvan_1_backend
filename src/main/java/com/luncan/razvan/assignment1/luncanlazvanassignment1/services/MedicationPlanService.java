package com.luncan.razvan.assignment1.luncanlazvanassignment1.services;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationPlanDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationPlanDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders.MedicationPlanBuilder;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Medication;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.MedicationPlan;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Patient;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.MedicalRepository;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.MedicationPlanRepository;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicalRepository medicalRepository;
    private final PatientRepository patientRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository,
                                 MedicalRepository medicalRepository,
                                 PatientRepository patientRepository){
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicalRepository = medicalRepository;
        this.patientRepository = patientRepository;
    }

    /*public List<MedicationPlanDTO> getMedicationPlans(Integer patientID){
        List<MedicationPlan> medicationPlans = patientRepository.getOne(patientID).get
        return medicationPlans.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }*/

    public Integer insert(MedicationPlanDetailsDTO medicationPlanDetailsDTO){
        Integer patientID = medicationPlanDetailsDTO.getPatientID();

        Patient patient = patientRepository.findById(patientID).get();

        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDetailsDTO);
        List<Integer> medicationIDsList = medicationPlanDetailsDTO.getMedicationIDsList();
        Set<Medication> medicationsList = new HashSet<>();

        //Populate list of medicine of medication plan
        for(Integer medicationID: medicationIDsList){
            Medication medication = medicalRepository.findById(medicationID).get();
            medicationPlan.getMedicationList().add(medication);
            //extract the list of medicine using the ids from
            //the frontend
            medicationsList.add(medication);
        }

        //set the patient for the medication plan
        medicationPlan.setMedicationPlanPatient(patient);

        //add the medication plan for the patient
        patient.getMedicationPlans().add(medicationPlan);

        //for each medicine assign the medication plan
        for(Medication medication: medicationsList){
            medication.getMedicationPlans().add(medicationPlan);
            medicalRepository.save(medication);
        }


        patientRepository.save(patient);

        MedicationPlan medicationPlanInserted = medicationPlanRepository.save(medicationPlan);

        return medicationPlanInserted.getId();
    }

}
