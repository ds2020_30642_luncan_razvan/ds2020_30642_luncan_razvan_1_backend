package com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Patient;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class CareGiverDTO extends UserDTO{

    private List<Patient> patients;

    public CareGiverDTO(String name, Date birthDate, String gender, String address, String role, String userName, String password){
        super(name, birthDate, gender, address, role, userName, password);
        this.patients = new ArrayList<>();
    }

    public CareGiverDTO(int id, String name, Date birthDate, String gender, String address, String role){
        super(id, name, birthDate, gender, address, role);
        this.patients = new ArrayList<>();
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CareGiverDTO that = (CareGiverDTO) o;
        return Objects.equals(patients, that.patients) && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), patients);
    }
}
