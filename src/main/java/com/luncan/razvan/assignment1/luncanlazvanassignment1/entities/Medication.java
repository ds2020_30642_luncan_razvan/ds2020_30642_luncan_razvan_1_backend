package com.luncan.razvan.assignment1.luncanlazvanassignment1.entities;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;


@Entity
public class Medication implements Serializable {

    private static final long SERIAL_VERSION_UID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "sideeffects", nullable = false)
    private String sideEffects;

    @Column(name = "dosage", nullable = false)
    private int dosage;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToMany(mappedBy = "medicationList", fetch = FetchType.EAGER)
    private Set<MedicationPlan> medicationPlans;

    public Medication(){}

    public Medication(String name, String sideEffects, int dosage){
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getDosage() {
        return dosage;
    }
    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public Set<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(Set<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    @Override
    public String toString() {
        return "Medication{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sideEffects='" + sideEffects + '\'' +
                ", dosage=" + dosage +
                ", medicationPlans=" + medicationPlans +
                '}';
    }
}
