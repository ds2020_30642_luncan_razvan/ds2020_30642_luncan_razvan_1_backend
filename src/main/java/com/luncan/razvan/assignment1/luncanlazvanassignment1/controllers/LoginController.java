package com.luncan.razvan.assignment1.luncanlazvanassignment1.controllers;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.LoginDetailsDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.CareGiver;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Patient;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.User;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.CareGiverRepository;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.PatientRepository;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/home")
public class LoginController {

    private final PatientRepository patientRepository;
    private final CareGiverRepository careGiverRepository;

    @Autowired
    public LoginController(PatientRepository patientRepository, CareGiverRepository careGiverRepository){
        this.patientRepository = patientRepository;
        this.careGiverRepository = careGiverRepository;
    }

    @PostMapping()
    public ResponseEntity<StringBuilder> getUserAccount(@RequestBody LoginDetailsDetailsDTO loginDetailsDetailsDTO){
        String userName = loginDetailsDetailsDTO.getUserNameLogin();
        String password = loginDetailsDetailsDTO.getPasswordLogin();

        Patient patient = patientRepository.findPatientByUserNameAndPassword(userName, password);
        CareGiver careGiver = careGiverRepository.findCareGiverByUserNameAndPassword(userName, password);

        if(careGiver != null){
            int careGiverID = careGiver.getId();

            StringBuilder linkToCareGiver = new StringBuilder("/caregiver/careGiverPatients/");
            linkToCareGiver.append(careGiverID);

            return new ResponseEntity<>(linkToCareGiver, HttpStatus.OK);
        }else if(patient != null){
            int patientID = patient.getId();
            StringBuilder linkToPatientMedication = new StringBuilder("/patient/medicationPlanPatients/");
            linkToPatientMedication.append(patientID);
            return new ResponseEntity<>(linkToPatientMedication, HttpStatus.OK);
        }

        return new ResponseEntity<>(new StringBuilder(), HttpStatus.OK);
    }

}
