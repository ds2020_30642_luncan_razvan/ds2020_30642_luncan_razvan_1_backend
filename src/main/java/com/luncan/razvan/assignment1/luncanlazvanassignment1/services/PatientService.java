package com.luncan.razvan.assignment1.luncanlazvanassignment1.services;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.CareGiverPatientDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationPlanDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.PatientDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.PatientDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders.MedicationPlanBuilder;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders.PatientBuilder;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.CareGiver;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.MedicationPlan;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Patient;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.CareGiverRepository;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.MedicationPlanRepository;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;
    private final CareGiverRepository careGiverRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, CareGiverRepository careGiverRepository){
        this.patientRepository = patientRepository;
        this.careGiverRepository = careGiverRepository;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    public List<PatientDTO> findPatients(){
        List<Patient> patients = patientRepository.findAll();
        return patients.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public Integer insert(PatientDetailsDTO patientDetailsDTO){
        Patient patient = PatientBuilder.toEntity(patientDetailsDTO);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public List<MedicationPlanDTO> getMedicationPlanFromPatientByID(Integer patientID){
        List<MedicationPlan> medicationPlans = patientRepository.findById(patientID).get().getMedicationPlans();
        return medicationPlans.stream().filter(distinctByKey(MedicationPlan::getId))
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    public Integer update(PatientDetailsDTO patientDetailsDTO){
        Patient patient = PatientBuilder.toEntityUpdate(patientDetailsDTO);
        Patient patient1 = patientRepository.findById(patient.getId()).get();
        patient.setMedicationPlans(patient1.getMedicationPlans());
        patient.setRole(patient1.getRole());
        patient.setCareGiver(patient1.getCareGiver());
        patient.setUserName(patient1.getUserName());
        patient.setPassword(patient1.getPassword());
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was updates in db", patient.getId());
        return patient.getId();
    }

    public void deletePatientByID(int id){
        patientRepository.deleteById(id);
    }

    public Integer assignCareGiver(CareGiverPatientDetailsDTO careGiverPatientDetailsDTO){
        Integer careGiverID = careGiverPatientDetailsDTO.getCareGiverID();
        Integer patientID = careGiverPatientDetailsDTO.getPatientID();

        CareGiver careGiver = careGiverRepository.findById(careGiverID).get();
        Patient patient = patientRepository.findById(patientID).get();

        careGiver.getPatients().add(patient);
        patient.setCareGiver(careGiver);

        patientRepository.save(patient);
        careGiverRepository.save(careGiver);
        return careGiverID;
    }


}
