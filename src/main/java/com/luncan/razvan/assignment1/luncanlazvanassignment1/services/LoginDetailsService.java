package com.luncan.razvan.assignment1.luncanlazvanassignment1.services;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.LoginDetailsDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.User;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CareGiverService.class);
    private final UserRepository userRepository;

    @Autowired
    public LoginDetailsService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public String findUserForLogin(LoginDetailsDetailsDTO loginDetailsDetailsDTO){
        /*User user = userRepository.findUsersByUserNameAndPassword(loginDetailsDetailsDTO.getUserNameLogin(), loginDetailsDetailsDTO.getPasswordLogin());
        if(user != null){
            if(user.getRole().equals("caregiver")){
                return "/caregiver/careGiverPatients/" + user.getId();
            }
        }*/
        return null;
    }

}
