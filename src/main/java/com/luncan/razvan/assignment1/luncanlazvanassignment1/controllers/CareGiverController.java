package com.luncan.razvan.assignment1.luncanlazvanassignment1.controllers;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.CareGiverDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.CareGiverDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.PatientDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.PatientDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.services.CareGiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/caregiver")
public class CareGiverController {

    private final CareGiverService careGiverService;

    @Autowired
    public CareGiverController(CareGiverService careGiverService){this.careGiverService = careGiverService;}

    @GetMapping()
    public ResponseEntity<List<CareGiverDTO>> getCareGivers(){
        List<CareGiverDTO> dtos = careGiverService.findCareGivers();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Integer> insertCareGiver(@Valid @RequestBody CareGiverDetailsDTO careGiverDetailsDTO){
        Integer careGiverID = careGiverService.insert(careGiverDetailsDTO);
        return new ResponseEntity<>(careGiverID, HttpStatus.CREATED);
    }

    @RequestMapping(method = PUT)
    public ResponseEntity<Integer> updatePatient(@Valid @RequestBody CareGiverDetailsDTO careGiverDetailsDTO){
        Integer careGiverID = careGiverService.update(careGiverDetailsDTO);
        return new ResponseEntity<>(careGiverID, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity<Integer> deleteCareGiver(@PathVariable("id") Integer careGiverID){
        careGiverService.deleteCareGiverByID(careGiverID);
        return new ResponseEntity<>(careGiverID, HttpStatus.OK);
    }

    @GetMapping(value="/careGiverPatients/{id}")
    public ResponseEntity<List<PatientDTO>> getCareGiversPatients(@PathVariable("id") Integer careGiverID){
        List<PatientDTO> patientDTOS = careGiverService.getCareGiversPatients(careGiverID);
        return new ResponseEntity<>(patientDTOS, HttpStatus.OK);
    }

}
