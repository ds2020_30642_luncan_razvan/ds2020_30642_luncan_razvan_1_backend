package com.luncan.razvan.assignment1.luncanlazvanassignment1.entities;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class MedicationPlan implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name="intake_intervals", nullable = false)
    private String intakeIntervals;

    @Column(name="beginning_treatment", nullable = false)
    private Date beginningOfTreatment;

    @Column(name="ending_treatment", nullable = false)
    private Date endingOfTreatment;

    @ManyToMany(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(
            name = "medication_medication_plan",
            joinColumns = @JoinColumn(name = "medication_plan_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_id")
    )
    private Set<Medication> medicationList;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "medication_plan_patient_id")
    private Patient medicationPlanPatient;

    public MedicationPlan(){

    }

    public MedicationPlan(String intakeIntervals, Date beginningOfTreatment, Date endingOfTreatment) {
        this.intakeIntervals = intakeIntervals;
        this.beginningOfTreatment = beginningOfTreatment;
        this.endingOfTreatment = endingOfTreatment;
        this.medicationList = new HashSet<>();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public Date getBeginningOfTreatment() {
        return beginningOfTreatment;
    }

    public void setBeginningOfTreatment(Date beginningOfTreatment) {
        this.beginningOfTreatment = beginningOfTreatment;
    }

    public Date getEndingOfTreatment() {
        return endingOfTreatment;
    }

    public void setEndingOfTreatment(Date endingOfTreatment) {
        this.endingOfTreatment = endingOfTreatment;
    }

    public Set<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(Set<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public Patient getMedicationPlanPatient() {
        return medicationPlanPatient;
    }

    public void setMedicationPlanPatient(Patient medicationPlanPatient) {
        this.medicationPlanPatient = medicationPlanPatient;
    }
}


