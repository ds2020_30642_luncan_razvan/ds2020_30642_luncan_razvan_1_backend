package com.luncan.razvan.assignment1.luncanlazvanassignment1.services;

import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.MedicationDetailsDTO;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.dtos.builders.MedicationBuilder;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.entities.Medication;
import com.luncan.razvan.assignment1.luncanlazvanassignment1.repositories.MedicalRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicalRepository medicalRepository;

    @Autowired
    public MedicationService(MedicalRepository medicalRepository){this.medicalRepository = medicalRepository;}

    public List<MedicationDTO> findMedication(){
        List<Medication> medications = medicalRepository.findAll();
        return medications.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationDetailsDTO medicationDetailsDTO){
        Medication medication = MedicationBuilder.toEntity(medicationDetailsDTO);
        medication = medicalRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    public Integer update(MedicationDetailsDTO medicationDetailsDTO){
        Integer medicationID = medicationDetailsDTO.getId();
        Medication medication = MedicationBuilder.toEntityUpdate(medicationDetailsDTO);
        Medication oldMedication = medicalRepository.getOne(medicationID);
        medication.setMedicationPlans(oldMedication.getMedicationPlans());
        medication = medicalRepository.save(medication);
        LOGGER.debug("Medication with id {} was updates in db", medication.getId());
        return medication.getId();
    }

    public void deleteMedicationByID(int id){medicalRepository.deleteById(id);}

}
